﻿# cli-template sample

This sample generates a single html file from a single markdown file with the default template.

The command that is executed is `m4d --file README.md --output README.html --template material`. You can run the command from the package.json with `npm install` and `npm run generate`. This command takes the `README.md` and generates the file `README.html`.
