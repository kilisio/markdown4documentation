﻿require('./src/prototypes');

module.exports = {
  Configuration: require('./src/configuration'),
  HtmlBuilder: require('./src/htmlBuilder'),
  HtmlParser: require('./src/htmlParser'),
  Processor: require('./src/processor'),
  Templates: {
    default: require('./templates/default'),
    material: require('./templates/material')
  }
};
