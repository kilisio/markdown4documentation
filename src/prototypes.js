﻿String.prototype.consistsOf = function (char) {
    for (let i = 0; i < this.length; i++) {
        if (this[i] !== char)
            return false;
    }
    return true;
};
String.prototype.trimChar = function (char) {
    if (this.consistsOf(char))
        return '';
    let begin = 0;
    let end = 0;
    for (let i = 0; i < this.length; i++) {
        if (this[i] === char)
            begin++;
        else
            break;
    }
    for (let i = this.length - 1; i >= 0; i--) {
        if (this[i] === char)
            end++;
        else
            break;
    }
    return this.substr(begin, this.length - begin - end);
};
String.prototype.startsWithListElement = function (type) {
    if (type === 'ul' && (this.startsWith('*') || this.startsWith('+') || this.startsWith('-')) && (this[1] === ' ' || this[1] === '\t'))
        return true;
    else if (type === 'ol') {
        const pointIndex = this.indexOf('.');
        if (pointIndex === -1 || pointIndex === 0)
            return false;
        const firstPart = this.substr(0, pointIndex);
        if (firstPart.onlyNumbers())
            return true;
    }
    return false;
};
String.prototype.trimListElement = function (type) {
    if (type === 'ul' && (this.startsWith('* ') || this.startsWith('+ ') || this.startsWith('- ')))
        return this.substr(2);
    else if (type === 'ol' && this.startsWithListElement('ol')) {
        const pointIndex = this.indexOf('.');
        return this.substr(pointIndex + 2);
    }
    return this;
};
String.prototype.onlyNumbers = function () {
    const numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    for (let char of this)
        if (!numbers.includes(char))
            return false;
    return true;
};
String.prototype.isURL = function () {
    let regex = /^[a-z]:\/\/.*$/i;
    return this.match(regex) !== null;
};
String.prototype.isEmail = function () {
    let regex = /^[^@]+@[^@/]+/i;
    return this.match(regex) !== null;
};
String.prototype.trimStartMax = function (maxLength) {
    let part1 = this.substr(0, maxLength);
    let part2 = this.substr(maxLength);
    return part1.trimStart() + part2;
};
String.prototype.lonelyTags = function () {
    let tags = {};

    let startIndex = -1;
    let inTag = false;
    let opening = false;
    let inTagName = false;
    let tagName = '';
    let inQuote;
    for (let i = 0; i < this.length; i++) {
        const char = this[i];
        const next = this[i + 1];
        if (inTag) {
            if (inTagName) {
                if (char === ' ' || char === '>') {
                    inTagName = false;
                    tagName = this.substring(startIndex + (opening ? 1 : 2), i);
                    inTag = char !== '>';
                }
            } else {
                if (inQuote) {
                    if (char === inQuote)
                        inQuote = undefined;
                } else {
                    if (char === '"' || char === '\'')
                        inQuote = char;
                    else if (char === '>')
                        inTag = false;
                }
            }
            if (!inTag) {
                if (!tags[tagName])
                    tags[tagName] = 0;
                if (opening)
                    tags[tagName]++;
                else
                    tags[tagName]--;

                startIndex = -1;
                opening = false;
                inTagName = false;
                tagName = '';
                inQuote = undefined;
            }
        } else {
            if (char === '<' && next !== ' ' && (next !== '/' || this[i + 2] !== ' ')) {
                inTag = true;
                inTagName = true;
                opening = next !== '/';
                startIndex = i;
            }
        }
    }

    let result = {
        opening: [],
        closing: []
    };
    for(const entry of Object.entries(tags))
        if (entry[1] < 0)
            result.closing.push(entry[0]);
        else if(entry[1] > 0)
            result.opening.push(entry[0]);
    return result;
};
String.prototype.stripAll = function(stripChar) {
    let result = '';
    for(let char of this)
        if (char !== stripChar)
            result += char;
    return result;
};

Array.prototype.last = function () {
    return this[this.length - 1];
};
Array.prototype.removeEmptyEntries = function () {
    let result = [];
    for (let elem of this)
        if (elem !== '')
            result.push(elem);
    return result;
};
Array.prototype.trim = function () {
    if (this.removeEmptyEntries().length === 0)
        return [];
    let begin = 0;
    let end = 0;
    for (let i = 0; i < this.length; i++) {
        if (this[i].trim() === '')
            begin++;
        else
            break;
    }
    for (let i = this.length - 1; i >= 0; i--) {
        if (this[i].trim() === '')
            end++;
        else
            break;
    }
    return this.slice(begin, this.length - end);
};
Array.prototype.startsWith = function(searchArray) {
    if (this.length < searchArray.length)
        return false;
    for (let i = 0; i < searchArray.length; i++) {
        if (this[i] !== searchArray[i])
            return false;
    }
    return true;
};
Array.prototype.trimBom = function(){
    let bomMarker = [
        ['\xEF', '\xBB', '\xBF', 239, 187, 191]
    ];
    for(const bom of bomMarker) {
        if(this.startsWith(bom)) {
            return this.slice(bom.length);
        }
    }
    return this;
};
