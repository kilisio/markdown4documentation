class CliOptions {

    #availableTypes = ['html', 'pdf'];

    #type;
    #file;
    #output;
    #template;

    #configuration;

    constructor(parameters) {
        if (!parameters || parameters.length === 0)
            throw new Error('Required parameters missing');
        const fs = require('fs');
        for (let i = 0; i < parameters.length; i++) {
            const lastParameter = (i + 1) === parameters.length;
            switch (parameters[i]) {
                case '-f':
                case '--file':
                    if (lastParameter)
                        throw new Error('Missing value for parameter file');
                    i++;
                    const file = parameters[i];
                    if (!fs.existsSync(file) || !fs.lstatSync(file).isFile())
                        throw `'${file}' is not a file`;
                    this.#file = file;
                    break;
                case '-o':
                case '--output':
                    if (lastParameter)
                        throw new Error('Missing value for parameter output');
                    i++;
                    const output = parameters[i];
                    this.#output = output;
                    break;
                case '-t':
                case '--type':
                    if (lastParameter)
                        throw new Error('Missing value for parameter type');
                    i++;
                    if (!this.#availableTypes.includes(parameters[i]))
                        throw `Unknown type '${parameters[i]}'`;
                    this.#type = parameters[i];
                    break;
                case '--template':
                    if (lastParameter)
                        throw new Error('Missing value for parameter template');
                    i++;
                    this.#template = parameters[i];
                    break;
                case '-c':
                case '--configuration':
                    if (lastParameter)
                        throw new Error('Missing value for parameter configuration');
                    i++;
                    const configFile = parameters[i];
                    if (!fs.existsSync(configFile) || !fs.lstatSync(configFile).isFile())
                        throw `'${configFile}' is not a file`;
                    this.#configuration = this.#parseConfigurationFile(configFile);
                    break;
                default:
                    throw new Error(`Unknown parameter '${parameters[i]}'`);
            }
        }
        if (this.#configuration) {
            if (this.#file || this.#output || this.#type || this.#template)
                throw new Error('If configuration is set then no other parameter is allowed');
        } else {
            if (!this.#file)
                throw new Error('Required parameter --file missing');
            if (!this.#output)
                throw new Error('Required parameter --output missing');
            if (!this.#type)
                this.#type = 'html';

            const Configuration = require('./configuration');
            this.#configuration = new Configuration();
            const job = this.#configuration.CreateJob();
            job.Type = this.#type;
            job.Output = this.#output;
            job.Template = this.#template;

            const task = new JobFileTask();
            task.File = this.#file;
            job.Input.push(task);

            if (!this.#checkTemplate(job.Template))
                throw new Error(`Unknown template '${job.Template}'`);
        }
    }

    get Configuration() {
        return this.#configuration;
    }

    #parseConfigurationFile = (file) => {
        const fs = require('fs');
        const path = require('path');
        const Configuration = require('./configuration');

        const configFile = fs.readFileSync(file, {encoding: 'utf8'});
        const configJson = JSON.parse(configFile);

        const configuration = new Configuration();

        for (const jsonJob of configJson['jobs']) {
            let mode = 'file';
            let type = 'html';
            let template = 'default';
            let suppressLeadingH1 = false;
            let headingNumbering = false;

            if (jsonJob['mode'])
                mode = jsonJob['mode'];
            if (jsonJob['type'])
                type = jsonJob['type'];
            if (jsonJob['template'])
                template = jsonJob['template'];
            if (jsonJob['suppressLeadingH1'])
                suppressLeadingH1 = jsonJob['suppressLeadingH1'];
            if (jsonJob['headingNumbering'])
                headingNumbering = jsonJob['headingNumbering'];

            if (!this.#checkTemplate(template))
                throw new Error(`Unknown template '${template}'`);
            if (mode !== 'file' && mode !== 'directory' && mode !== 'merge')
                throw new Error(`Unsupported mode '${mode}'`);
            if (type !== 'html' && type !== 'pdf')
                throw new Error(`Unsupported file type '${type}'`);

            if (jsonJob['assets']) {
                const assetJob = configuration.CreateJob();
                assetJob.Type = '{copy}';
                const input = path.resolve(jsonJob['assets']);
                if (!fs.existsSync(input) || !fs.lstatSync(input).isDirectory())
                    throw `'${input}' is not a directory`;
                assetJob.Input = input;
                let output = path.resolve(jsonJob['output']);
                if (mode === 'file' || mode === 'merge')
                    output = path.dirname(output);
                assetJob.Output = path.resolve(output, path.basename(input));
            }

            switch (mode) {
                case 'file':
                    if (!fs.existsSync(jsonJob['input']) || !fs.lstatSync(jsonJob['input']).isFile())
                        throw `'${jsonJob['input']}' is not a file`;
                    if (fs.existsSync(jsonJob['output']) && !fs.lstatSync(jsonJob['output']).isFile())
                        throw `'${jsonJob['output']}' is not a file`;
                    if (!fs.existsSync(path.dirname(jsonJob['output'])))
                        fs.mkdirSync(path.dirname(jsonJob['output']), {recursive: true});
                    const fileJob = configuration.CreateJob();
                    fileJob.Type = type;
                    fileJob.Template = template;
                    fileJob.Output = jsonJob['output'];
                    fileJob.HeadingNumbering = headingNumbering;
                    this.#parsePdfOptions(fileJob, jsonJob);
                    const task = new JobFileTask();
                    task.File = jsonJob['input'];
                    task.SuppressLeadingH1 = suppressLeadingH1;
                    fileJob.Input.push(task);
                    break;
                case 'directory':
                    const input = jsonJob['input'];
                    const output = jsonJob['output'];
                    if (!fs.existsSync(input) || !fs.lstatSync(input).isDirectory())
                        throw `'${input}' is not a directory`;
                    if (!fs.existsSync(output))
                    fs.mkdirSync(output, {recursive: true});
                    if (!fs.lstatSync(output).isDirectory())
                        throw `'${output}' is not a directory`;
                    const fileList = fs.readdirSync(input);
                    for(const file of fileList)
                        if (file.endsWith('.md')) {
                            const fileJob = configuration.CreateJob();
                            fileJob.Type = type;
                            fileJob.Template = template;
                            fileJob.Output = path.resolve(output, file.substring(0, file.length - 2) + type);
                            fileJob.HeadingNumbering = headingNumbering;
                            this.#parsePdfOptions(fileJob, jsonJob);
                            const task = new JobFileTask();
                            task.File = path.resolve(input, file);
                            task.SuppressLeadingH1 = suppressLeadingH1;
                            fileJob.Input.push(task);
                        }
                    break;
                case 'merge':
                    if (fs.existsSync(jsonJob['output']) && !fs.lstatSync(jsonJob['output']).isFile())
                        throw `'${jsonJob['output']}' is not a file`;
                    if (!fs.existsSync(path.dirname(jsonJob['output'])))
                        fs.mkdirSync(path.dirname(jsonJob['output']), {recursive: true});
                    const mergeJob = configuration.CreateJob();
                    mergeJob.Type = type;
                    mergeJob.Template = template;
                    mergeJob.Output = jsonJob['output'];
                    mergeJob.HeadingNumbering = headingNumbering;
                    this.#parsePdfOptions(mergeJob, jsonJob);

                    for (const jsonTask of jsonJob['input']) {
                        const task = this.#parseTask(jsonTask);
                        mergeJob.Input.push(task);
                    }
                    break;
                default:
                    throw new Error(`Unsupported mode '${mode}'`);
            }

            if (jsonJob['assets'] && type === 'pdf') {
                const deleteJob = configuration.CreateJob();
                deleteJob.Type = '{delete}';
                let output = path.resolve(jsonJob['output']);
                if (mode === 'file' || mode === 'merge')
                    output = path.dirname(output);
                const input = path.resolve(jsonJob['assets']);
                deleteJob.Input = path.resolve(output, path.basename(input));
            }
        }
        return configuration;
    };

    #parsePdfOptions = (job, jsonJob) => {
        if (jsonJob["pdfOptions"]) {
            const pdfOptions = jsonJob["pdfOptions"];
            if (pdfOptions["width"])
                job.PdfOptions.Width = pdfOptions["width"];
            if (pdfOptions["height"])
                job.PdfOptions.Height = pdfOptions["height"];
            if (pdfOptions["format"])
                job.PdfOptions.Format = pdfOptions["format"];
            else if (pdfOptions["height"] && pdfOptions["width"])
                job.PdfOptions.Format = undefined;
            if (pdfOptions["margin"]) {
                job.PdfOptions.Margin.Top = pdfOptions["margin"]["top"];
                job.PdfOptions.Margin.Right = pdfOptions["margin"]["right"];
                job.PdfOptions.Margin.Bottom = pdfOptions["margin"]["bottom"];
                job.PdfOptions.Margin.Left = pdfOptions["margin"]["left"];
            }
            if (pdfOptions["displayHeaderFooter"])
                job.PdfOptions.DisplayHeaderFooter = pdfOptions["displayHeaderFooter"];
            if (pdfOptions["landscape"])
                job.PdfOptions.Landscape = pdfOptions["landscape"];
            if (pdfOptions["headerTemplate"])
                job.PdfOptions.HeaderTemplate = pdfOptions["headerTemplate"];
            else
                job.PdfOptions.HeaderTemplate = "<span></span>";
            if (pdfOptions["footerTemplate"])
                job.PdfOptions.FooterTemplate = pdfOptions["footerTemplate"];
            else
                job.PdfOptions.FooterTemplate = "<span></span>";
        }
    };

    #checkTemplate = (template) => {
        const fs = require('fs');
        const path = require('path');

        if (!template)
            return true;

        try {
            require('../templates/' + template);
            return true;
        } catch {
            template = path.resolve(template);
            if (fs.existsSync(template) && fs.lstatSync(template).isFile()) {
                try {
                    require(template);
                    return true;
                } catch (e) {
                    return false;
                }
            } else {
                return false;
            }
        }
    };

    #parseTask = (jsonTask) => {
        const fs = require('fs');
        let suppressLeadingH1 = false;
        let indentHeadlines = 0;
        let type = 'file';

        if (jsonTask['suppressLeadingH1'])
            suppressLeadingH1 = jsonTask['suppressLeadingH1'];
        if (jsonTask['indentHeadlines'])
            indentHeadlines = jsonTask['indentHeadlines'];
        if (jsonTask['type'])
            type = jsonTask['type'];

        switch (type) {
            case 'file':
                if ((!jsonTask['file'] || !fs.existsSync(jsonTask['file']) || !fs.lstatSync(jsonTask['file']).isFile()))
                    throw new Error(`'${jsonTask['file']}' is not a file`);
                break;
            case 'heading':
                if (!jsonTask['content'])
                    throw new Error('Property "content" cannot be empty on "type" "heading"');
                break;
            default:
                throw new Error(`Unsupported type '${type}'`);
        }

        let task = new JobFileTask();
        task.Type = type;
        task.File = jsonTask['file'];
        task.Content = jsonTask['content'];
        task.SuppressLeadingH1 = suppressLeadingH1;
        task.IndentHeadlines = indentHeadlines;

        if (jsonTask['sub'])
            for (const subTask of jsonTask['sub']) {
                const sub = this.#parseTask(subTask);
                task.Sub.push(sub);
            }

        return task;
    };
}

module.exports = CliOptions;

class JobFileTask {
    Type = 'file';
    File;
    SuppressLeadingH1 = false;
    IndentHeadlines = 0;
    Sub = [];
}
