﻿# Contributing

Thank you for considering contributing to markdown4documentation. No matter if you want to report a bug, suggest a feature or even code, you are contributing something valuable to this project and helping many other people.

By following these guidelines, you will help to improve the quality of the contributions. As a result, they can be processed more quickly and the overall quality of the project increases.

* [What can I contribute?](#what-can-i-contribute)
* [Rules for contributing](#rules-for-contributing)
    * [Report an issue](#report-an-issue)
    * [Contribute code](#contribute-code)

## What can I contribute?

Markdown is a great tool that is used in many different areas. There are also many extensions to the standard. Are you missing a feature of a Markdown extension, for example, GitHub Flavored Markdown? Or the HTML and PDF output could use some more cool features? Then feel free to extend markdown4documentation and create a merge request.

You would like to help others to use this tool better or make it more easy to use? Please write small examples that I can add to the documentation.

You want to contribute something, but don't know what? You like to look at the open [Issues][issues] and find a point that is on `To Do`. But only take points that have the label `To Do` and are not assigned to someone else already. If you start coding on an issue, please leave a comment there so that other people now that someone is working on it.

[issues]: https://gitlab.com/vekunz/markdown4documentation/issues

## Rules for contributing

It should be self-evident that we maintain a friendly tone in everything we do. Even if the code owner disagrees with you at times, it should always remain objective.

### Report an issue

**If you find a security vulnerability, do NOT open an issue. Email git@veit-kunz.de instead.**

If you have found that markdown4documentation makes a mistake in parsing, feel free to create an issue. Please do not use the issue tracker to ask general markdown questions. There are many sites and forums on the internet where there is an answer to almost every question.

Please attach all the information to the issue so that the bug can be reproduced directly. Please add the markdown, the HTML section created by markdown4documentation and the HTML you would expect. Please limit the examples to the relevant areas, otherwise, it will get confusing very quickly.

### Contribute code

Do you want to contribute directly to markdown4documentation with code? Very cool! But please follow a few rules to make sure your merge request is not rejected because of small things.

* **Branch from develop.** The branch for continuous development is `develop`. The `master` branch only contains releases. 

* **Use proper branch names.** If you want to contribute a new feature or improvement, use a feature branch `feature/#<issue>-<name>`, if you want to fix a bug, use a bug branch `bug/#<issue>-<name>`. `<issue>` is the issue number of GitLab. You can see the issue number either in the issue overview or in the URL if the issue is open.

* **Use an issue.** When you fix a bug or write a feature, please use an existing issue or create a new one if none exists.

* **Follow the program structure.** markdown4documentation processes documents line by line, block by block. Please keep this structure and do not break out of it. If at some point it is no longer possible, or perhaps it makes more sense, please discuss this in an issue with the team before.

* **Maintain the API.** Should changes to the API be necessary, please keep them as minor as possible. Describe your changes in detail in the Merge Request.

* **No new classes.** Please do not add new classes unless it is necessary. When you extend classes, be careful not to add new public methods to the public classes described in the README, which do not have to be public.

* **Write unit tests.** Even if the original code is not fully tested, please write unit tests for the code you add. markdown4documentation uses the framework Jest for unit tests. Tests are located in the `test` folder and are split per file. In a test file, the tests are roughly structured by function.

* **Maintain the changelog.** If you add features or fix bugs, please also expand the changelog in the 'Next Release' section.
