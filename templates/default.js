﻿DEFAULT_TEMPLATE = {
  html: `<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
</head>
<body>
\${CONTENT}
</body>
</html>
`,

  h1: '<h1${ATTRIBUTES}>${CONTENT}</h1>',
  h2: '<h2${ATTRIBUTES}>${CONTENT}</h2>',
  h3: '<h3${ATTRIBUTES}>${CONTENT}</h3>',
  h4: '<h4${ATTRIBUTES}>${CONTENT}</h4>',
  h5: '<h5${ATTRIBUTES}>${CONTENT}</h5>',
  h6: '<h6${ATTRIBUTES}>${CONTENT}</h6>',

  p: '<p>${CONTENT}</p>',

  blockquote: '<blockquote>${CONTENT}</blockquote>',

  pre: '<pre>${CONTENT}</pre>',
  code: '<code>${CONTENT}</code>',

  ul: '<ul>${CONTENT}</ul>',
  ol: '<ol>${CONTENT}</ol>',
  li: '<li>${CONTENT}</li>',

  em: '<em>${CONTENT}</em>',
  strong: '<strong>${CONTENT}</strong>',

  hr: '<hr />',

  a: '<a${ATTRIBUTES}>${CONTENT}</a>',
  img: '<img${ATTRIBUTES} />',

  hNumbering: {
    level1: '${l1}. ',
    level2: '${l1}.${l2}. ',
    level3: '${l1}.${l2}.${l3}. ',
    level4: '${l1}.${l2}.${l3}.${l4}. ',
    level5: '${l1}.${l2}.${l3}.${l4}.${l5}. ',
    level6: '${l1}.${l2}.${l3}.${l4}.${l5}.${l6}. '
  },
};

module.exports = DEFAULT_TEMPLATE;
