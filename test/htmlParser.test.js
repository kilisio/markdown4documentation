const HtmlParser = jest.requireActual('../src/htmlParser');
const HtmlBuilder = jest.requireActual('../src/htmlBuilder');

class MockHtmlBuilder {
    constructor(templates) { }
    BuildTag(tag, attributes, content) {
        if (['hr'].includes(tag))
            return `<${tag} />`;
        else
            return `<${tag}>${content}</${tag}>`;
    }
    BuildPage(content) { return `<html>${content}</html>`; }
}

describe('Parse single block elements', () => {
    test.each([
        ['h1 1', '# Headline', '<h1>Headline</h1>'],
        ['h1 2', 'Headline\r\n===', '<h1>Headline</h1>'],
        ['h2 1', '## Headline', '<h2>Headline</h2>'],
        ['h2 2', 'Headline\r\n---', '<h2>Headline</h2>'],
        ['h3', '### Headline', '<h3>Headline</h3>'],
        ['h4', '#### Headline', '<h4>Headline</h4>'],
        ['h5', '##### Headline', '<h5>Headline</h5>'],
        ['h6', '###### Headline', '<h6>Headline</h6>'],
        ['p Single line', 'Paragraph', '<p>Paragraph</p>'],
        ['p Multi line', 'Paragraph\r\nLine 2', '<p>Paragraph Line 2</p>'],
        ['blockquote Single line', '>Blockquote', '<blockquote>Blockquote</blockquote>'],
        ['blockquote Multi line 1', '>Blockquote\r\n>Line 2', '<blockquote>Blockquote Line 2</blockquote>'],
        ['blockquote Multi line 2', '>Blockquote\r\nLine 2', '<blockquote>Blockquote Line 2</blockquote>'],
        ['code Single line', '    code', '<pre><code>code</code></pre>'],
        ['code Multi line 1', '    code\r\n    line 2', '<pre><code>code\r\nline 2</code></pre>'],
        ['code Multi line 2', '    code\r\n\r\n    line 2', '<pre><code>code\r\n\r\nline 2</code></pre>'],
        ['code Fences', '```\r\ncode\r\n```', '<pre><code>code</code></pre>'],
        ['hr * 1', '***', '<hr />'],
        ['hr * 2', '* * *', '<hr />'],
        ['hr - 1', '---', '<hr />'],
        ['hr - 2', '- - -', '<hr />'],
        ['html 1', '<div>\r\n\r\nText\r\n\r\n</div>', '<div>\r\n\r\nText\r\n\r\n</div>'],
        ['html 2', '<div>\r\n\r\n<div>Text</div>\r\n\r\n</div>', '<div>\r\n\r\n<div>Text</div>\r\n\r\n</div>'],
        ['html 3', '<div\r\n>Text</div\r\n>', '<div\r\n>Text</div\r\n>'],
    ])('%s', (title, markdown, expected) => {
        const parser = new HtmlParser(markdown, new MockHtmlBuilder());
        const result = parser.Parse();

        expect(result.trim()).toEqual(expected);
    });

    describe('ul', () => {
        describe.each(['*', '+', '-'])('%s', (elm) => {
            test.each([
                ['Single line', `${elm} element`, '<ul><li>element</li></ul>'],
                ['Multi line', `${elm} element\r\nline 2`, '<ul><li>element line 2</li></ul>'],
                ['Multi elements', `${elm} element\r\n${elm} element 2`, '<ul><li>element</li> <li>element 2</li></ul>'],
            ])('%s', (title, markdown, expected) => {
                const parser = new HtmlParser(markdown, new MockHtmlBuilder());
                const result = parser.Parse();

                expect(result.trim()).toEqual(expected);
            });
        });
    });
    describe('ol', () => {
        test.each([
            ['Single line', '1. element', '<ol><li>element</li></ol>'],
            ['Multi line', '23. element\r\nline 2', '<ol><li>element line 2</li></ol>'],
            ['Multi elements 1', '1. element\r\n2. element 2', '<ol><li>element</li> <li>element 2</li></ol>'],
            ['Multi elements 2', '1. element\r\n1. element 2', '<ol><li>element</li> <li>element 2</li></ol>'],
        ])('%s', (title, markdown, expected) => {
            const parser = new HtmlParser(markdown, new MockHtmlBuilder());
            const result = parser.Parse();

            expect(result.trim()).toEqual(expected);
        });
    });
});

describe('Parse multiple elements', () => {
    test.each([
        ['h1 & p 1', '# Headline\r\nParagraph', '<h1>Headline</h1>\r\n<p>Paragraph</p>'],
        ['h1 & p 2', 'Headline\r\n===\r\nParagraph', '<h1>Headline</h1>\r\n<p>Paragraph</p>'],
        ['h2 & p 1', '## Headline\r\nParagraph', '<h2>Headline</h2>\r\n<p>Paragraph</p>'],
        ['h2 & p 2', 'Headline\r\n---\r\nParagraph', '<h2>Headline</h2>\r\n<p>Paragraph</p>'],
        ['h3 & p', '### Headline\r\nParagraph', '<h3>Headline</h3>\r\n<p>Paragraph</p>'],
        ['h4 & p', '#### Headline\r\nParagraph', '<h4>Headline</h4>\r\n<p>Paragraph</p>'],
        ['h5 & p', '##### Headline\r\nParagraph', '<h5>Headline</h5>\r\n<p>Paragraph</p>'],
        ['h6 & p', '###### Headline\r\nParagraph', '<h6>Headline</h6>\r\n<p>Paragraph</p>'],
        ['p & p 1', 'Paragraph 1\r\n\r\nParagraph 2', '<p>Paragraph 1</p>\r\n<p>Paragraph 2</p>'],
        ['p & code', 'Paragraph\r\n\r\n    code', '<p>Paragraph</p>\r\n<pre><code>code</code></pre>'],
        ['code & p 1', '    code\r\n\r\nParagraph', '<pre><code>code</code></pre>\r\n<p>Paragraph</p>'],
        ['code & p 2', '    code\r\nParagraph', '<pre><code>code</code></pre>\r\n<p>Paragraph</p>'],
        ['ul & p', '* List\r\n\r\nParagraph', '<ul><li>List</li></ul>\r\n<p>Paragraph</p>'],
        ['blockquote & p', '> Blockquote\r\n\r\nParagraph', '<blockquote>Blockquote</blockquote>\r\n<p>Paragraph</p>'],
        ['p & h1', 'Paragraph\r\n# Headline', '<p>Paragraph</p>\r\n<h1>Headline</h1>'],
        ['p & code', 'Paragraph\r\n\r\n    code', '<p>Paragraph</p>\r\n<pre><code>code</code></pre>'],
        ['p & blockquote', 'Paragraph\r\n> Blockquote', '<p>Paragraph</p>\r\n<blockquote>Blockquote</blockquote>'],
        ['code indent && code fences', '    code 1\r\n```\r\ncode 2\r\n```', '<pre><code>code 1</code></pre>\r\n<pre><code>code 2</code></pre>'],
    ])('%s', (title, markdown, expected) => {
        const parser = new HtmlParser(markdown, new MockHtmlBuilder());
        const result = parser.Parse();

        expect(result.trim()).toEqual(expected);
    });
});

describe('Parse block elements in block elements', () => {
    test.each([
        ['blockquote - p & p', '> Paragraph 1\r\n>\r\n> Paragraph 2', '<blockquote><p>Paragraph 1</p>\r\n<p>Paragraph 2</p></blockquote>'],
        ['blockquote - code', '>     code', '<blockquote><pre><code>code</code></pre></blockquote>'],
        ['blockquote - ul', '> * List', '<blockquote><ul><li>List</li></ul></blockquote>'],
        ['blockquote - h1', '> # Headline', '<blockquote><h1>Headline</h1></blockquote>'],
        ['blockquote - blockquote', '> > Blockquote', '<blockquote><blockquote>Blockquote</blockquote></blockquote>'],
        ['ul - p & p', '* Paragraph 1\r\n\r\n    Paragraph 2', '<ul><li><p>Paragraph 1</p>\r\n<p>Paragraph 2</p></li></ul>'],
        ['ul - p & p', '* Paragraph 1\r\n\r\n\tParagraph 2', '<ul><li><p>Paragraph 1</p>\r\n<p>Paragraph 2</p></li></ul>'],
        ['ul - code', '*     code', '<ul><li><pre><code>code</code></pre></li></ul>'],
        ['ul - ul', '* * List', '<ul><li><ul><li>List</li></ul></li></ul>'],
        ['ul - h1', '* # Headline', '<ul><li><h1>Headline</h1></li></ul>'],
        ['ul - blockquote', '* > Blockquote', '<ul><li><blockquote>Blockquote</blockquote></li></ul>'],
    ])('%s', (title, markdown, expected) => {
        const parser = new HtmlParser(markdown, new MockHtmlBuilder());
        const result = parser.Parse();

        expect(result.trim()).toEqual(expected);
    });
});

describe('Parse document', () => {

    test('HeadingNumbering true', () => {
        const markdown = `
# Test
# Test
## Test
## Test
### Test
### Test
## Test
### Test
# Test
## Test
### Test
# Test`;
        const expected = `<h1 id="test">1. Test</h1>\r\n<h1 id="test">2. Test</h1>\r\n<h2 id="test">2.1. Test</h2>\r\n<h2 id="test">2.2. Test</h2>\r\n<h3 id="test">2.2.1. Test</h3>\r\n<h3 id="test">2.2.2. Test</h3>\r\n<h2 id="test">2.3. Test</h2>\r\n<h3 id="test">2.3.1. Test</h3>\r\n<h1 id="test">3. Test</h1>\r\n<h2 id="test">3.1. Test</h2>\r\n<h3 id="test">3.1.1. Test</h3>\r\n<h1 id="test">4. Test</h1>`;
        const builder = new HtmlBuilder({});
        builder.EnableHNumbering(true);
        const parser = new HtmlParser(markdown, builder);
        const result = parser.Parse();

        expect(result.trim()).toEqual(expected);
    });

    test('HeadingNumbering false', () => {
        const markdown = `
# Test
# Test
## Test
## Test
### Test
### Test
## Test
### Test
# Test
## Test
### Test
# Test`;
        const expected = `<h1 id="test">Test</h1>\r\n<h1 id="test">Test</h1>\r\n<h2 id="test">Test</h2>\r\n<h2 id="test">Test</h2>\r\n<h3 id="test">Test</h3>\r\n<h3 id="test">Test</h3>\r\n<h2 id="test">Test</h2>\r\n<h3 id="test">Test</h3>\r\n<h1 id="test">Test</h1>\r\n<h2 id="test">Test</h2>\r\n<h3 id="test">Test</h3>\r\n<h1 id="test">Test</h1>`;
        const builder = new HtmlBuilder({});
        const parser = new HtmlParser(markdown, builder);
        const result = parser.Parse();

        expect(result.trim()).toEqual(expected);
    });

});
