const Configuration = jest.requireActual('../src/configuration');

test('Create configuration', () => {
    const configuration = new Configuration();
    expect(configuration).toBeDefined();
});

test('Create new Job', () => {
    const configuration = new Configuration();
    const job = configuration.CreateJob();

    expect(configuration.Jobs.length).toEqual(1);
    expect(job).toBeDefined();
    expect(job.Type).toEqual('html');
    expect(job.Input).toEqual([]);
    expect(job.Output).not.toBeDefined();
    expect(job.Template).not.toBeDefined();
});

test('Set Job parameter', () => {
    const configuration = new Configuration();
    const job = configuration.CreateJob();

    job.Mode = 'mode';
    job.Type = 'type';
    job.Input = 'input';
    job.Output = 'output';
    job.Template = 'template';

    expect(configuration.Jobs[0].Mode).toEqual('mode');
    expect(configuration.Jobs[0].Type).toEqual('type');
    expect(configuration.Jobs[0].Input).toEqual('input');
    expect(configuration.Jobs[0].Output).toEqual('output');
    expect(configuration.Jobs[0].Template).toEqual('template');

});

test('Create new Task', () => {
    const configuration = new Configuration();
    const task = configuration.CreateJob().CreateTask();

    expect(configuration.Jobs[0].Input).toHaveLength(1);
    expect(task).toBeDefined();
    expect(task.Type).toEqual('file');
    expect(task.File).not.toBeDefined();
    expect(task.SuppressLeadingH1).toBeFalsy();
    expect(task.IndentHeadlines).toEqual(0);
    expect(task.Sub).toEqual([]);
});

test('Create Sub Task', () => {
    const configuration = new Configuration();
    const subTask = configuration.CreateJob().CreateTask().CreateSubTask();

    expect(configuration.Jobs[0].Input[0].Sub).toHaveLength(1);
    expect(subTask).toBeDefined();
    expect(subTask.Type).toEqual('file');
    expect(subTask.File).not.toBeDefined();
    expect(subTask.SuppressLeadingH1).toBeFalsy();
    expect(subTask.IndentHeadlines).toEqual(0);
    expect(subTask.Sub).toEqual([]);
});
